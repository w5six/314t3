<?php 
    session_start();
    //redirect to log in page if not logged in
    if (!isset($_SESSION['loggedIN']))
	{
        header('Location: login.php');
        exit();
    }
    //logs out if tried to visit from wrong userType
    if($_SESSION['userType'] != "admin")
	{
        header('Location: logout.php');
        exit();
    }
    if (isset($_POST['searched']))
	{
        $connection = new mysqli('localhost', 'root', '','testestdb');

        $search = $connection->real_escape_string($_POST['searchPHP']);
		
		$getUser = $connection->query("SELECT * FROM user WHERE NRIC = '$search'");

        if($getUser->num_rows > 0)
		{
			if($getUser->num_rows > 0)
			{
				$row = $getUser->fetch_assoc();
				$_SESSION['user_user_id'] = $row['user_id'];
				$_SESSION['user_password'] = $row['password'];
				$_SESSION['user_NRIC'] = $row['NRIC'];
				$_SESSION['user_name'] = $row['name'];
				$_SESSION['user_DOB'] = $row['DOB'];
				$_SESSION['user_email'] = $row['email'];
				$_SESSION['user_phone'] = $row['phone_number'];
				$_SESSION['user_role'] = $row['role'];
				
				if($_SESSION['user_role'] == 'doctor')
				{
					$user_details = $connection->query("SELECT * FROM doctor LEFT JOIN user ON user.user_id = doctor.user_id = (SELECT user_id FROM user WHERE NRIC = '$search')");
					$rowD = $user_details->fetch_assoc();
					$_SESSION['doctor_doctor_id'] = $rowD['doctor_id'];
					$_SESSION['doctor_license'] = $rowD['license'];
					exit('found');
				}
				elseif($_SESSION['user_role'] == 'patient')
				{
					$user_details = $connection->query("SELECT * FROM patient LEFT JOIN user ON user.user_id = patient.user_id = (SELECT user_id FROM user WHERE NRIC = '$search')");
					$rowPt = $user_details->fetch_assoc();
					$_SESSION['patient_patient_id'] = $rowPt['patient_id'];
					$_SESSION['patient_patient_id'] = $rowPt['patient_id'];
					$_SESSION['patient_weight'] = $rowPt['weight'];
					$_SESSION['patient_height'] = $rowPt['height'];
					$_SESSION['patient_gender'] = $rowPt['gender'];
					$_SESSION['patient_address'] = $rowPt['address'];
					$_SESSION['patient_allergy'] = $rowPt['allergy'];
					exit('found');
				}
				elseif($_SESSION['user_role'] == 'pharmacist')
				{
					$user_details = $connection->query("SELECT * FROM pharmacist LEFT JOIN user ON user.user_id = pharmacist.user_id = (SELECT user_id FROM user WHERE NRIC = '$search')");
					$rowPh = $user_details->fetch_assoc();
					$_SESSION['pharmacist_pharmacist_id'] = $rowPh['pharmacist_id'];
					$_SESSION['pharmacist_license'] = $rowPh['license'];
					exit('found');
				}
				elseif($_SESSION['user_role'] == 'admin')
				{
					$user_details = $connection->query("SELECT * FROM admin LEFT JOIN user ON user.user_id = admin.user_id = (SELECT user_id FROM user WHERE NRIC = '$search')");
					$rowAd = $user_details->fetch_assoc();
					$_SESSION['pharmacist_pharmacist_id'] = $rowAd['pharmacist_id'];
					exit('found');
				}
				else
				{exit('found');}
			}
			else
			{exit('failed');}
		}
		else
		{exit('failed');}
            
        exit($search);
    }

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href=".\css\main.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function (){
            console.log('page ready: AdminHome');
            $("#searchButton").on('click', function (){
                var search = $(".search__input").val();
                if(search == "")
				{alert('Please fill in the values');}
				else
				{
                    $.ajax(
                    {
                        url: 'adminHome.php',
                        method: 'POST',
                        data:{
                            searched: 1,
                            searchPHP: search
                        },
                        success: function(response)
						{
                            $("#response").html(response);

                            if(response.indexOf('found') >= 0)
							{window.location = 'adminUserList.php';}
							else
							{alert("Search not found. Please try again");}        
                        }, dataType: 'text'
                    }
					);
                }
                console.log(search);
            });
			
        });
	</script>
</head>
<body>
	<header>
		<nav>
			<img src="img\admin-with-cogwheels.png" alt="admin" style="width:7%">
			<div class="nav__name"><h2><?php echo("{$_SESSION['name']}"); ?></h2></div>
			<ul>
				<li><a href="logout.php">Log out</a></li>
			</ul>
		</nav>
	</header>
	
	</br></br>
 
    <div class="container">
        <input type="text" name="searchBox" class="search__input" placeholder="Search for user..." style="width: 50%; margin-left: 20%">
        <button type="button" class="search__button" id="searchButton">Search</button>
        <button type="button" class="add__button--account" id="addAccountButton"><a href="adminAddAccount.php">Add Account</a></button>
    </div>

 
</body>
</html>