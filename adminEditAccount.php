<?php 
    session_start();
	//redirect to log in page if not logged in
    if (!isset($_SESSION['loggedIN']))
	{
        header('Location: login.php');
        exit();
    }
    
    //logs out if tried to visit from wrong userType
    if($_SESSION['userType'] != "admin")
	{
        header('Location: logout.php');
        exit();
    }

	$editID = $_SESSION['user_user_id'];

	$connection = new mysqli('localhost', 'root', '','testestdb');

	$getUser = $connection->query("SELECT * FROM user WHERE user_id = '$editID'");
	
	if($getUser->num_rows > 0){
		$row = $getUser->fetch_assoc();
		$password = $row['password'];
		$NRIC = $row['NRIC'];
		$name = $row['name'];
		$DOB = $row['DOB'];
		$email = $row['email'];
		$phone = $row['phone_number'];
		$role = $row['role'];
	}else{
		echo("NOTHING FOUND");
	}
	
    if (isset($_POST['editPHP'])){
        $connection = new mysqli('localhost', 'root', '','testestdb');

        $userPassword = $_POST['passwordPHP'];
        $userNRIC = $_POST['NRICPHP'];
        $userName = $_POST['namePHP'];
        $userDOB = $_POST['DOBPHP'];
        $userEmail = $_POST['emailPHP'];
        $userPhone = $_POST['phonePHP'];
        $userRole = $_POST['rolePHP'];
		
        if($connection->query("UPDATE user SET password = '$userpassword', NRIC = $userNRIC,name='$userName',DOB='$userDOB',email='$userEmail',phone_number='$userPhone',role='$userRole' WHERE user_id = $editID;") === TRUE){
            exit('editSuccess');
        }else{
            exit('failed');
        }
        
    }
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href=".\css\main.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
			
            console.log('page ready: AdminViewAccount');
			
			 $("#backButton").on('click', function (){

               $.ajax(
                    {
                        url: 'adminAddAccount.php',
                        method: 'POST',
                        data:{
                            back: 1
                        },
                        success: function(response){
                            $("#response").html(response);

                            if(response.indexOf('back') >= 0){

                                window.location = 'adminHome.php';
                                
                            }else{
                                alert("Please try again");
                            }
                            
                        },
                        dataType: 'text'
                    }
               );
            });
			
            $("#submitButton").on('click', function(event){  

                var NRIC = $("#user--NRIC").val();
                var name = $("#user--name").val();
                var DOB = $("#user--DOB").val();
                var email = $("#user--email").val();
                var phone = $("#user--phone").val();
				var role = $("#user--role").val();
                
                $.ajax(
                    {
                    url: 'adminEditAccount.php',
                    method: 'POST',
                    data:{
                        submitPHP: 1,
                        NRICPHP: NRIC,
                        namePHP: name,
                        DOBPHP: DOB,
                        emailPHP: email,
                        phonePHP: phone,
						rolePHP: role
                    },
                    success: function(response){
                        $("#response").html(response);

                        if(response.indexOf('editSuccess') >= 0){
                            window.location = 'adminViewAccount.php';
                        }else{
                            alert("Please try again");
                        }
                    },dataType: 'text'
                });
            });
                
        });

        </script>
    
</head>
<body>
    <nav>
        <img src="img\admin-with-cogwheels.png" alt="admin" style="width:7%">
        <div class="nav__name"><h2> <?php echo("{$_SESSION['name']}"); ?></h2></div>
        <ul>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>

    <button type="button" class="back__button" id="backButton">Back</button>

	<div class="container">
		<div class="box1">
			<div class="input__Text">User ID : </div>
			<div class="input__Text">Password : </div>
			<div class="input__Text">NRIC : </div>
			<div class="input__Text">Name : </div>
			<div class="input__Text">Date Of Birth : </div>
			<div class="input__Text">Email Address : </div>
			<div class="input__Text">Phone Number : </div>
			<div class="input__Text">Role : </div>
		</div>
		<div class="box2">
			<form method="POST">    
				<input type="text" name="user--user_id" class="medicine__input" id="user--user_id"required="required" disabled <?php echo"value='$editID'"?>>
				<input type="text" name="user--password" class="medicine__input" id="user--password"required="required"<?php echo"value='$password'"?>>
				<input type="text" name="user--NRIC" class="medicine__input" id="user--NRIC"required="required" <?php echo"value='$NRIC'"?>>
				<input type="text" name="user--name" class="medicine__input" id="user--name"required="required"<?php echo"value='$name'"?>>
				<input type="date" name="user--DOB" class="medicine__input" id="user--DOB"required="required"<?php echo"value='$DOB'"?>>
				<input type="text" name="user--email" class="medicine__input" id="user--email"required="required"<?php echo"value='$email'"?>>
				<input type="text" name="user--phone" class="medicine__input" id="user--phone"required="required"<?php echo"value='$phone'"?>>
				<input type="text" name="user--role" class="medicine__input" id="user--role"required="required"<?php echo"value='$role'"?> disabled>
			</form>
			<button id="submitButton" name="submit" class="edit__button" type="button" >Submit</button>
		</div>
    </div>
</body>
</html>
